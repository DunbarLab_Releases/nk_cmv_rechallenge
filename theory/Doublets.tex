\documentclass[12pt]{amsart}
\usepackage{amsmath}
\usepackage[lite]{amsrefs}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage[all,cmtip]{xy}
\usepackage{graphicx}
\usepackage{tikz-cd} 
\let\objectstyle=\displaystyle

\DeclareMathOperator*{\argmax}{argmax}
\DeclareMathOperator*{\Binom}{Binom}
\DeclareMathOperator*{\Multinom}{Multinom}
\DeclareMathOperator*{\Prob}{P}

\theoremstyle{definition}
\newtheorem{definition}{Definition}[section]

\theoremstyle{lemma}
\newtheorem{lemma}{Lemma}[section]
 
\theoremstyle{remark}
\newtheorem*{remark}{Remark}

\title{Analysis of Hash Tag Oligonucleotides to Identify Doublets}
\date{November 2020}
\author{Dunbar Group}

\begin{document}
\maketitle

\section{Introduction}
Hash Tag Oligonucleotides (HTOs) have found use in droplet-based single cell genomics for the labeling of multiplexed samples and the identification of droplets containing two or more cell (`doublets').
Demultiplexing of samples labeled by hashtag oligonucleotides is clear whenever a cell index is found exclusively in association with a single HTO.
The recovery of multiple HTOs associated to a single cell index signals the presence of a droplet containing cells from two or more different samples (a `heterotypic doublet').
HTOs do not offer a direct way to identify droplts containing cells from the same sample (`homotypic multiplets'); however, the rate of heterotypic multiplets, obtained from the sequencing of samples multiplexed via HTO, can be used to infer the expected overal rates of multiplets.
In this note, we sketch a very simple method to carry this out.

\section{Statistical Model}
We assume that a Poisson process governs the numbers of beads, $N_B$, and cells, $N_C$ present in droplets.
\begin{equation}
\begin{split}
P \left ( k_B = N_B, \lambda_B \right ) =& \frac{\lambda_B^{N_B}}{N_B!} e^{-\lambda_B} \\
P \left ( k_C = N_C, \lambda_C \right ) =& \frac{\lambda_C^{N_C}}{N_C!} e^{-\lambda_C}
\end{split}
\end{equation}
where $\lambda_B$ and $\lambda_C$ are the expected numbers of beads and barcodes in each cell.
We assume the processes are independent, in which case
\begin{equation}
P \left ( k_C = N_C \vert k_B = 1;  \lambda_C \right ) = \frac{\lambda_C^{N_C}}{N_C!} e^{-\lambda_C}
\end{equation}
The probability of {\it any} (homotypic or heterotypic) doublet is then given by
\begin{equation}
\begin{split}
P &\left ( k_C = 2;  \lambda_C \right ) \\
&= P \left ( k_1 = 2, k_2 = 0;  \lambda_C \right ) + P \left ( k_1 = 1, k_2 = 1;  \lambda_C \right ) + P \left ( k_1 = 0, k_2 = 2; \lambda_C \right )  \\
&= \frac{\lambda_C^2}{2!} e^{-\lambda_C}
\end{split}
\end{equation}
where $k_1$ and $k_2$ are the numbers of cells in the droplet from samples 1 and 2, respectively.

If the fraction of cells in populations 1 and 2 is $f_1$ and $f_2$, respectively, then
\begin{equation}
\begin{split}
P& \left ( k_1 = 2, k_2 = 0;  \lambda_C, f_1, f_2 \right ) \\
&+ P \left ( k_1 = 1, k_2 = 1;  \lambda_C, f_1, f_2 \right ) + P \left ( k_1 = 0, k_2 = 2;  \lambda_C, f_1, f_2 \right ) \\
&= \frac{(f_1 + f_2)^2 \lambda_C^2}{2!} e^{-\lambda_C}
\end{split}
\end{equation}
Expanding both sides, we see that
\begin{equation}
P \left ( k_1 = 1 \cap k_2 = 1; \lambda_C, f_1, f_2 \right ) = f_1 f_2 \lambda_C^2 e^{-\lambda_C}
\end{equation}
Thus the overall rate of doublets is related to the rate of heterotypic doublets as follows
\begin{equation}
P \left ( k_C = 2 \vert k_B = 1;  \lambda_C \right ) = \frac{1}{f_1 f_2} P \left ( k_1 = 1 \cap k_2 = 1; \lambda_C, f_1, f_2 \right )
\end{equation}
or
\begin{equation}
\textrm{Overall Doublet Rate} = \frac{1}{f_1 f_2} \cdot \textrm{Heterotypic Doublet Rate}
\end{equation}
This equation can also be expressed in terms of population counts of cells tagged with HTOs.  Let $p_1$ $p_2$ be the number of singlets from cell types 1 and 2, respectively; let $d$ be the number of heterotypic doublets; finally let $n$ be the number of HTO-null cells.
Then the fraction of cells in population 1 is given by
\begin{equation}
f_1 = \frac{p_1 + d}{p_1 + p_2 + 2d}
\end{equation}
while the fraction of cells in population 2 is given by
\begin{equation}
f_2 = \frac{p_2 + d}{p_1 + p_2 + 2d}
\end{equation}
The rate of heterotypic doublets is
\begin{equation}
\textrm{Heterotypic Doublet Rate} = \frac{d}{p_1 + p_2 + n + 2d}
\end{equation}
and the overall double rate is
\begin{equation}
\textrm{Overall Doublet Rate} = \frac{(p_1 + p_2 + 2d)^2 d}{(p_1 + d)(p_2 + d)(p_1 + p_2 + n + 2d)}
\end{equation}

Similarly the relationship between the rates of homotypic and heterotypic doublets is given by
\begin{equation}
\textrm{Homotypic Doublet Rate} = \frac{f_1^2 + f_2^2}{2 f_1 f_2} \cdot \textrm{Heterotypic Doublet Rate}
\end{equation}


\end{document}